#include <iostream>
#include "object.hpp"

using namespace std;

class Object::PimplObject
{
public:
    PimplObject()
    {
        cout << "Konstruktor PimplObject" << endl;
    }

    ~PimplObject()
    {
        cout << "Destruktor PimplObject" << endl;
    }

    void do_use()
    {
        cout << "from pimpl - do_use()" << endl;
    }
};

Object::Object() : pimpl_(new PimplObject())
{
    cout << "Konstruktor Object" << endl;
}

Object::~Object()
{
    cout << "Destruktor Object" << endl;
}

void Object::use()
{
    pimpl_->do_use();
}




