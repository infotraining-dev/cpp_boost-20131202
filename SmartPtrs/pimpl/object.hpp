#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <boost/scoped_ptr.hpp>

class Object
{
    class PimplObject;

    boost::scoped_ptr<PimplObject> pimpl_;

public:
    Object();
    ~Object();
    void use();
};

#endif
