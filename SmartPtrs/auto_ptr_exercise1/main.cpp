#include <memory>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <cassert>
#include <boost/scoped_ptr.hpp>

using namespace std;

class X
{
public:
	// konstruktor
	X(int value = 0)
		: value_(value)
	{
		std::cout << "Konstruktor X(" << value_ << ")\n"; 
	}
	
	// destruktor
	~X() 
	{
		std::cout << "Destruktor ~X(" << value_ << ")\n"; 
	}
	
    int value() const
	{
		return value_;
	}

    void set_value(int value)
	{
        value_ = value;
	}

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
	int value_;
};

void legacy_code(X* ptr)
{
    ptr->set_value(5);
    cout << "legacy_code: new value = " << ptr->value() << endl;
}

std::auto_ptr<X> factory(int arg) // TODO: poprawa z wykorzystaniem auto_ptr
{
    return std::auto_ptr<X>(new X(arg));
}

void unsafe1()  // TODO: poprawa z wykorzystaniem auto_ptr
{
    std::auto_ptr<X> ptrX(factory(4));

    /* kod korzystajacy z ptrX */

    legacy_code(ptrX.get());

    ptrX->unsafe();

    std::auto_ptr<X> copy_of_ptrx = ptrX;

    assert(ptrX.get() == 0);
    assert(copy_of_ptrx.get() != 0);

    copy_of_ptrx.reset();  // wczesniejsze zwolnienie

    // scoped_ptr
    boost::scoped_ptr<X> sc_ptr(factory(5));

    sc_ptr->unsafe();
}

namespace CPP11
{
    std::unique_ptr<X> factory(int arg) // TODO: poprawa z wykorzystaniem auto_ptr
    {
        std::unique_ptr<X> result(new X(arg));
        return result;  // unique_ptr<X>(unique_ptr<X>&& source)
    }

    void unsafe1()  // TODO: poprawa z wykorzystaniem auto_ptr
    {
        std::unique_ptr<X> ptrX(factory(4));

        /* kod korzystajacy z ptrX */

        legacy_code(ptrX.get());

        ptrX->unsafe();

        std::unique_ptr<X> moved_ptrx = std::move(ptrX); // jawny transfer wlasnosci

        assert(ptrX.get() == 0);
        assert(moved_ptrx.get() != 0);

        moved_ptrx.reset();  // wczesniejsze zwolnienie
    }
}

int main() try
{
    unsafe1();
}
catch(...)
{
    std::cout << "Zlapalem wyjatek!" << std::endl;
}
