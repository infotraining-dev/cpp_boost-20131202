#include <iostream>
#include <vector>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;

class Mutex
{
public:
    void lock()
    {
        cout << "Mutex locked" << endl;
    }

    void unlock()
    {
        cout << "Mutex unlocked" << endl;
    }
};

// RAII handle
class LockGuard : boost::noncopyable
{
    Mutex& mtx_;
public:
    LockGuard(Mutex& mtx) : mtx_(mtx)
    {
        mtx_.lock();
    }

    ~LockGuard()
    {
        mtx_.unlock();
    }
};

class Monitor
{
    mutable Mutex mtx_;
    vector<int> buffer_;
public:
    void use() const
    {
       LockGuard lk(mtx_);

       cout << buffer_.at(13) << endl ;
    }

    void do_stuff()
    {
        mtx_.lock();
        boost::shared_ptr<Mutex>
                lk(&mtx_, [](Mutex* mtx) { mtx->unlock();});

        cout << buffer_.at(13) << endl ;
    }
};

int main()
try
{
    Monitor m;

    //m.use();

    m.do_stuff();

    return 0;
}
catch(...)
{
    cout << "Zlapalem wyjatek." << endl;
}

