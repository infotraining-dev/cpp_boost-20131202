#include <iostream>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <cassert>
#include <cstdlib>
#include <stdexcept>
#include <boost/make_shared.hpp>

class X
{
public:
    // konstruktor
    X(int value = 0)
        : value_(value)
    {
        std::cout << "Konstruktor X(" << value_ << ")\n";
    }

    // destruktor
    ~X()
    {
        std::cout << "Destruktor ~X(" << value_ << ")\n";
    }

    int value() const
    {
        return value_;
    }

    void set_value(int value)
    {
        value_ = value;
    }

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
    int value_;
};

class Base
{
public:
    virtual void do_stuff()
    {
        std::cout << "Base::do_stuff()" << std::endl;
    }

    virtual ~Base() {}
};

class Derived : public Base
{
public:
    virtual void do_stuff()
    {
        std::cout << "Derived::do_stuff()" << std::endl;
    }

    void specific()
    {
        std::cout << "Derived::specific()" << std::endl;
    }
};

boost::shared_ptr<Base> create(const std::string& id)
{
    if (id == "Base")
        return boost::shared_ptr<Base>(new Base());
    else if (id == "Derived")
        return boost::shared_ptr<Derived>(new Derived());

    throw std::runtime_error("Bad id");
}


class A
{
	boost::shared_ptr<X> x_;
public:
	A(boost::shared_ptr<X> x) : x_(x) {}
	
	int get_value()
	{
		return x_->value();
	}
};

class B
{
	boost::shared_ptr<X> x_;
public:
	B(boost::shared_ptr<X> x) : x_(x) {}
	
	void set_value(int i)
	{
        x_->set_value(i);
	}	
};

void foo(boost::shared_ptr<Base> objA, boost::shared_ptr<Base> objB, int x)
{
    objA.reset();
    objB.reset();
    std::cout << x << std::endl;
}

int gen()
{
    return 4;
}

int main()
{
	{
        boost::shared_ptr<X> spX1 = boost::make_shared<X>();
		std::cout << "RC: " << spX1.use_count() << std::endl;
		{
			boost::shared_ptr<X> spX2(spX1);
			std::cout << "RC: " << spX2.use_count() << std::endl;
		}
		std::cout << "RC: " << spX1.use_count() << std::endl;
	}

	std::cout << "\n//----------------------\n\n";

    boost::shared_ptr<X> temp = boost::make_shared<X>(14);
    A a(temp);
    {
        B b(temp);
        b.set_value(28);
    }

    assert(a.get_value() == 28);
    std::cout << "a.get_value() = " << a.get_value() << std::endl;

    boost::shared_ptr<Base> ptr_base = create("Base");

    boost::shared_ptr<Derived> ptr_derived
            = boost::dynamic_pointer_cast<Derived>(ptr_base);

    if (ptr_derived)
        ptr_derived->specific();
    else
        std::cout << "ptr_derived points to Base object" << std::endl;

    // potencjalne wycieki dla argumentow tymczasowych
    boost::shared_ptr<Base> arg1(new Base);
    boost::shared_ptr<Base> arg2(new Base);
    foo(arg1, arg2, gen());
}
