#include <iostream>
#include <boost/cast.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

int main() try
{
    string number = "335";

    int x = boost::lexical_cast<int>(number);

    short sx = boost::numeric_cast<short>(x);

    string message = "sx = " + boost::lexical_cast<string>(sx);

    cout << message << endl;

    x = -8;
    unsigned int ux = boost::numeric_cast<unsigned int>(x);
    cout << "ux = " << ux << endl;

    return 0;
}
catch(const boost::bad_lexical_cast& e)
{
    cout << "Blad rzutowania leksykalnego: " << e.what() << endl;
}
catch(const boost::bad_numeric_cast& e)
{
    cout << "Blad: " << e.what() << endl;
}
