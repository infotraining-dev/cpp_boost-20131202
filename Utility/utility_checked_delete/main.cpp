#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <vector>
#include <algorithm>
#include <boost/checked_delete.hpp>

using namespace std;

class SomeClass;

SomeClass* create()
{
	return (SomeClass*)0;
}

void simple_test()
{
    SomeClass* p = create();

    delete p; // w najlepszym razie warning
}

void real_test()
{
    ToBeDeleted* tbd = new ToBeDeleted[10];

	Deleter exterminator;
    exterminator.delete_it(tbd); // wowalenie delete na rzecz typu niekompletnego
}

int main()
{
    vector<ToBeDeleted*> vec_pointers;

    vec_pointers.push_back(new ToBeDeleted);
    vec_pointers.push_back(new ToBeDeleted);

    for_each(vec_pointers.begin(), vec_pointers.end(),
             &boost::checked_delete<ToBeDeleted>);

    //real_test();
}
