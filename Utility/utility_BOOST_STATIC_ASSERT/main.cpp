#include <iostream>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>

class Base {};

class Derived : public Base {};

class Different
{
};

// definiowanie wlasnej cechy typu
template <typename T>
class IsDifferent : public boost::false_type
{
};

template <>
class IsDifferent<Different> : public boost::true_type
{
};



template <typename T>
class OnlyCompatibleWithIntegralTypes
{
	BOOST_STATIC_ASSERT(boost::is_integral<T>::value);
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
	BOOST_STATIC_ASSERT(sizeof(int)==4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
    BOOST_STATIC_ASSERT((boost::is_base_of<Base, T>::value)
                        || IsDifferent<T>::value);
}

template <typename T>
void foo(T arg)
{
    BOOST_STATIC_ASSERT(IsDifferent<T>::value);
}

int main()
{
    OnlyCompatibleWithIntegralTypes<int> test1;

    int arr[15];

	accepts_arrays_with_size_between_1_and_100(arr);

	Derived arg;
    Different arg_diff;
    works_with_base_and_derived(arg);

    foo(arg_diff);
}
