#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_pod.hpp>
#include <boost/type_traits/is_floating_point.hpp>
#include <list>
#include <vector>
#include <algorithm>
#include <iterator>
#include <boost/assign.hpp>

using namespace std;

void foo(int arg)
{
    cout << "foo(int)" << endl;
}

//template <typename T, typename T::nested_type* = nullptr>
//void foo(T arg)
//{
//    typedef typename T::nested_type NestedType;

//    NestedType var;

//    cout << "foo<T>(T)" << endl;
//}

template <typename T>
void foo(T arg, typename boost::disable_if<boost::is_integral<T> >::type* ptr = 0)
{
    cout << "template foo<T>" << endl;
}

//template <typename T>
//typename boost::disable_if<boost::is_integral<T> >::type foo(T arg)
//{
//    cout << "template foo<T>" << endl;
//}

class MyType
{
public:
    typedef int nested_type;
};

template <typename InIt, typename OutIt>
void mcopy(InIt start, InIt end, OutIt out)
{
    cout << "generic mcopy" << endl;

    for(InIt it = start; it != end; ++it)
        *out++ = *it;
}

template <typename T>
void mcopy(T* start, T* end, T* out, typename boost::enable_if<boost::is_pod<T> >::type* ptr = 0)
{
    cout << "mcopy with memcpy" << endl;

    memcpy(out, start, (end - start) * sizeof(T));
}

template <typename T, typename Enable = void>
class DataProcessor
{
public:
    void process_data(T arg)
    {
        cout << "generic DataProcessor" << endl;
    }
};

template <typename T>
class DataProcessor<T, typename boost::enable_if<boost::is_floating_point<T> >::type>
{
public:
    void process_data(T arg)
    {
        cout << "DataProcessor for floats" << endl;
    }
};


int main()
{
    using namespace boost::assign;

    list<string> words = list_of("One")("Two")("Three");

    vector<string> copy_of_words(words.size());

    mcopy(words.begin(), words.end(), copy_of_words.begin());

    mcopy(copy_of_words.begin(), copy_of_words.end(), ostream_iterator<string>(cout, " "));
    cout << "\n\n";

    int tab1[3] = { 1, 2, 3 };
    int tab2[3];

    mcopy(tab1, tab1 + 3, tab2);  // wywolanie mcopy

    mcopy(tab2, tab2 + 3, ostream_iterator<int>(cout, " "));
    cout << endl;

    cout << "\n\n";

    int x = 10;
    foo(x);

    short sx = 10;
    foo(sx);

    foo(3.14);

    MyType mt;
    foo(mt);

    DataProcessor<int> dp1;

    dp1.process_data(7);

    DataProcessor<double> dp2;

    dp2.process_data(8.99);
}

