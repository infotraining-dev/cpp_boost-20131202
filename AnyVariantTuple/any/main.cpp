#include <iostream>
#include <typeinfo>
#include <boost/any.hpp>

using namespace std;

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& name) : id_(id), name_(name)
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

int main()
{
    boost::any a1;

    if (a1.empty())
        cout << "a1 jest pusty" << endl;

    a1 = 3.14;
    cout << "a1.typename = " << a1.type().name() << endl;

    a1 = 55;
    cout << "a1.typename = " << a1.type().name() << endl;

    a1 = string("Text");
    cout << "a1.typename = " << a1.type().name() << endl;

    a1 = Person(1, "Kowalski");
    cout << "a1.typename = " << a1.type().name() << endl;

    boost::any a2 = a1;

    //a1 = 23;

    Person* ptr_p = boost::any_cast<Person>(&a1);

    if (ptr_p)
    {
        cout << "(*ptr_p) = name: " << ptr_p->name() << endl;
    }
    else
        cout << "Nieudana konwersja" << endl;

    try
    {
        Person p = boost::any_cast<Person>(a1);

        cout << "p = name: " << p.name() << endl;
    }
    catch(const boost::bad_any_cast& e)
    {
        cout << "Blad: " << e.what() << endl;
    }

    a1 = 15;

    int* x = boost::unsafe_any_cast<int>(&a1);

    cout << "x = " << *x << endl;
}

