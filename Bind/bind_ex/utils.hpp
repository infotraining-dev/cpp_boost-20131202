#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>
#include <string>

/* wypisywanie kontenerów na ekranie */
template <typename Container>
void print(const Container& container, const std::string& prefix = "")
{
	std::cout << prefix << "[ ";

	typename Container::const_iterator cit = container.begin();
	typename Container::const_iterator end = container.end();
	for( ; cit != end; ++cit)
		std::cout << *cit << " ";

	std::cout << "]\n";
}

template <typename FirstType, typename SecondType>
std::ostream& operator <<(std::ostream& out, const std::pair<FirstType, SecondType>& p)
{
	out << "(" << p.first << ", " << p.second << ")";

	return out;
}

/* klasa sledzaca operacje kopiowania */
class Tracer
{
	int id_;
	mutable int copy_constr_, assign_op_;
	static int counter_;
	static bool verbose_;
public:
	Tracer();
	~Tracer();
	Tracer(const Tracer& source);
	Tracer& operator=(const Tracer& source);
	int id() const;
	int copy_constr() const;
	int assign_op() const;
	
	static void set_verbose(bool verbose_flag);
};

std::ostream& operator <<(std::ostream& out, const Tracer& t);

#endif